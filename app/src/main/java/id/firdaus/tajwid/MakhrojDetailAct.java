package id.firdaus.tajwid;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import id.firdaus.tajwid.adapters.MakhrojDetailListAdapter;
import id.firdaus.tajwid.entities.Huruf;
import id.firdaus.tajwid.entities.Makhroj;

public class MakhrojDetailAct extends AppCompatActivity {

    ImageView ic_back;
    TextView titel_makhraj, deskripsi_makhraj;
    ImageView header_makhraj;

    private RecyclerView recycler_view;
    private List<Huruf> hurufList = new ArrayList<>();
    private MakhrojDetailListAdapter adapter;
    private String jenis_gambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makhroj_detail);

        ic_back = findViewById(R.id.ic_back);
        header_makhraj = findViewById(R.id.header_makhraj);
//        daftar_huruf= findViewById(R.id.daftar_huruf);
        titel_makhraj= findViewById(R.id.titel_makhraj);
        deskripsi_makhraj= findViewById(R.id.deskripsi_makhraj);
        recycler_view= findViewById(R.id.recycler_view);

        //mengambil string
        Bundle bundle = getIntent().getExtras();
        Makhroj makhroj = bundle.getParcelable("makhroj");
        jenis_gambar = bundle.getString("jenis_gambar");

        if (makhroj == null){
            startActivity(new Intent(this, HomeAct.class));
        }

        titel_makhraj.setText(makhroj.getNamaMakhroj());
        deskripsi_makhraj.setText(makhroj.getDescMakhroj());

        recycler_view.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_view.setLayoutManager(linearLayoutManager);

        try {
            String result = new HurufGet(makhroj.getIdMakhroj()).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private class HurufGet extends AsyncTask<Void, Integer, String> {
        private int id_makhroj;
        private Gson gson;

        public HurufGet(int id_makhroj) {
            this.id_makhroj = id_makhroj;
        }

        @Override
        protected String doInBackground(Void... voids) {
            URL url = null;
            HttpURLConnection urlConnection = null;
            InputStream in = null;
            try {
                String uri = getString(R.string.host)+"huruf_get_by_makhroj.php?id_makhroj="+id_makhroj;
                Log.d(MakhrojDetailAct.class.getSimpleName(), uri);

                url = new URL(uri);
                urlConnection = (HttpURLConnection) url.openConnection();
//                in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String json_String = "";
                String line;
                while ((line = rd.readLine()) != null) {
                    json_String += line;
                    Log.d(MakhrojDetailAct.class.getSimpleName(), json_String);
                }

                JSONArray jsonArray = new JSONArray(json_String);
                Log.d(MakhrojDetailAct.class.getSimpleName(), "jsonArray:"+jsonArray.toString());

                if (jsonArray != null && jsonArray.length() > 0){
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gson = gsonBuilder.create();
                    hurufList = Arrays.asList(gson.fromJson(jsonArray.toString(), Huruf[].class));
                    Log.d(MakhrojDetailAct.class.getSimpleName(), "hurufList:"+hurufList.size());

                    adapter = new MakhrojDetailListAdapter(MakhrojDetailAct.this, hurufList);
                }
//                readStream(in);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null){
                    urlConnection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            recycler_view.setAdapter(adapter);

            adapter.setOnItemClickListener(new MakhrojDetailListAdapter.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    Huruf huruf = hurufList.get(position);
                    Intent intent = new Intent(MakhrojDetailAct.this, HurufDetailAct.class);
                    intent.putExtra("huruf", huruf);
                    intent.putExtra("jenis_gambar", jenis_gambar);
                    startActivity(intent);
                }

                @Override
                public void onItemLongClick(int position, View v) {
                    Huruf huruf = hurufList.get(position);
                    Intent intent = new Intent(MakhrojDetailAct.this, HurufDetailAct.class);
                    intent.putExtra("huruf", huruf);
                    intent.putExtra("jenis_gambar", jenis_gambar);
                    startActivity(intent);
                }
            });

//            adapter.notifyDataSetChanged();
//            Log.d(MakhrojDetailAct.class.getSimpleName(), "getItemCount:"+adapter.getItemCount());
        }
    }
}
