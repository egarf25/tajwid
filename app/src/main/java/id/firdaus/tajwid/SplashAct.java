package id.firdaus.tajwid;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //syntak untuk setting timer selama 1 detik dari halaman splashscreen menuju halaman home
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //untuk berpindah ke activity lain
                Intent gotohome = new Intent(SplashAct.this, HomeAct.class);
                startActivity(gotohome);
                finish();


            }
        }, 1000);//waktu delay 1 detik
    }
}
