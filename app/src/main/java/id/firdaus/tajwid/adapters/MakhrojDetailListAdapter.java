package id.firdaus.tajwid.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import id.firdaus.tajwid.R;
import id.firdaus.tajwid.entities.Huruf;

public class MakhrojDetailListAdapter extends RecyclerView.Adapter<MakhrojDetailListAdapter.HurufViewHolder> {
    private static ClickListener clickListener;;
    private List<Huruf> list = new ArrayList<>();
    private Context context;

    public MakhrojDetailListAdapter(Context context, List<Huruf> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public HurufViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_huruf, viewGroup, false);
        ImageView imageHuruf = view.findViewById(R.id.image_huruf);
        TextView namaHuruf = view.findViewById(R.id.nama_huruf);
        TextView deskripsiHuruf = view.findViewById(R.id.deskripsi_huruf);

        HurufViewHolder viewHolder = new HurufViewHolder(view, imageHuruf, namaHuruf, deskripsiHuruf);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HurufViewHolder viewHolder, int i) {
//        viewHolder.bindHurufView(list.get(i));
        Huruf huruf = list.get(i);
        String urlHuruf = context.getString(R.string.host) +"uploads/" + huruf.getIconHuruf();
        Picasso.get().load(urlHuruf)
                .fit()
                .centerCrop()
                .into(viewHolder.imageHuruf);
        viewHolder.namaHuruf.setText(huruf.getNamaHuruf());
        viewHolder.deskripsiHuruf.setText(huruf.getDescHuruf());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HurufViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        ImageView imageHuruf;
        TextView namaHuruf;
        TextView deskripsiHuruf;

        private Context context;

        public HurufViewHolder(View view, ImageView imageHuruf, TextView namaHuruf, TextView deskripsiHuruf) {
            super(view);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);

            this.imageHuruf = imageHuruf;
            this.namaHuruf = namaHuruf;
            this.deskripsiHuruf = deskripsiHuruf;
        }

        public void bindHurufView(Huruf huruf) {
//            mNameTextView.setText(restaurant.getName());
//            mCategoryTextView.setText(restaurant.getCategories().get(0));
//            mRatingTextView.setText("Rating: " + restaurant.getRating() + "/5");
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition(), view);
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onItemLongClick(getAdapterPosition(), view);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        MakhrojDetailListAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}
