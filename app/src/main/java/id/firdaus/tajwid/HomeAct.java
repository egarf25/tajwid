package id.firdaus.tajwid;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.database.DatabaseReference;

import id.firdaus.tajwid.entities.Makhroj;

public class HomeAct extends AppCompatActivity {

    LinearLayout  btn_halqi, btn_khaisyum, btn_syafawi, btn_halqi2, btn_khaisyum2, btn_syafawi2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btn_halqi = findViewById(R.id.btn_halqi);
        btn_khaisyum = findViewById(R.id.btn_Khaisyum);
        btn_syafawi = findViewById(R.id.btn_syafawi);

        btn_halqi2 = findViewById(R.id.btn_halqi2);
        btn_khaisyum2 = findViewById(R.id.btn_Khaisyum2);
        btn_syafawi2 = findViewById(R.id.btn_syafawi2);

        Makhroj makhroj = new Makhroj();


        btn_halqi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMakhrojDetail(new Makhroj(1, "Halqi", "Tenggorokan"), "path");
            }
        });
        btn_khaisyum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMakhrojDetail(new Makhroj(2, "Khaisyum", "Rongga Hidung"), "path");
            }
        });
        btn_syafawi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMakhrojDetail(new Makhroj(3, "Syafawi", "Dua Bibir"), "path");
            }
        });


        btn_halqi2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMakhrojDetail(new Makhroj(1, "Halqi", "Tenggorokan"), "blob");
            }
        });
        btn_khaisyum2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMakhrojDetail(new Makhroj(2, "Khaisyum", "Rongga Hidung"), "blob");
            }
        });
        btn_syafawi2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMakhrojDetail(new Makhroj(3, "Syafawi", "Dua Bibir"), "blob");
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        checkApplicationPermissions();
    }

    private void gotoMakhrojDetail(Makhroj makhroj, String jenis_gambar){
        Intent intent = new Intent(HomeAct.this, MakhrojDetailAct.class);
        //meletakan data pada intent
        intent.putExtra("makhroj",makhroj);
        intent.putExtra("jenis_gambar",jenis_gambar);
        startActivity(intent);
    }

    private void checkApplicationPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (! ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 102);
            }
        }
    }
}
