package id.firdaus.tajwid.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class Makhroj implements Parcelable {
    private int idMakhroj;
    private String namaMakhroj;
    private String descMakhroj;

    public Makhroj() {
    }

    public Makhroj(int idMakhroj, String namaMakhroj, String descMakhroj) {
        this.idMakhroj = idMakhroj;
        this.namaMakhroj = namaMakhroj;
        this.descMakhroj = descMakhroj;
    }

    public int getIdMakhroj() {
        return idMakhroj;
    }

    public void setIdMakhroj(int idMakhroj) {
        this.idMakhroj = idMakhroj;
    }

    public String getNamaMakhroj() {
        return namaMakhroj;
    }

    public void setNamaMakhroj(String namaMakhroj) {
        this.namaMakhroj = namaMakhroj;
    }

    public String getDescMakhroj() {
        return descMakhroj;
    }

    public void setDescMakhroj(String descMakhroj) {
        this.descMakhroj = descMakhroj;
    }

    protected Makhroj(Parcel in) {
        idMakhroj = in.readInt();
        namaMakhroj = in.readString();
        descMakhroj = in.readString();
    }

    public static final Creator<Makhroj> CREATOR = new Creator<Makhroj>() {
        @Override
        public Makhroj createFromParcel(Parcel in) {
            return new Makhroj(in);
        }

        @Override
        public Makhroj[] newArray(int size) {
            return new Makhroj[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(idMakhroj);
        parcel.writeString(namaMakhroj);
        parcel.writeString(descMakhroj);
    }
}
