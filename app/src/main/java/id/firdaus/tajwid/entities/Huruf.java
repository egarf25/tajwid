package id.firdaus.tajwid.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Huruf implements Parcelable {

    @SerializedName("id_huruf")
    private int idHuruf;

    @SerializedName("nama_huruf")
    private String namaHuruf;

    @SerializedName("desc_huruf")
    private String descHuruf;

    @SerializedName("gambar_path")
    private String gambarPath;

    @SerializedName("gambar_blob")
    private String gambarBlob;

    @SerializedName("icon_huruf")
    private String iconHuruf;

    @SerializedName("id_makhroj")
    private int idMakhroj;

    public Huruf() {
    }

    protected Huruf(Parcel in) {
        idHuruf = in.readInt();
        namaHuruf = in.readString();
        descHuruf = in.readString();
        gambarPath = in.readString();
        gambarBlob = in.readString();
        iconHuruf = in.readString();
        idMakhroj = in.readInt();
    }

    public static final Creator<Huruf> CREATOR = new Creator<Huruf>() {
        @Override
        public Huruf createFromParcel(Parcel in) {
            return new Huruf(in);
        }

        @Override
        public Huruf[] newArray(int size) {
            return new Huruf[size];
        }
    };

    public int getIdHuruf() {
        return idHuruf;
    }

    public void setIdHuruf(int idHuruf) {
        this.idHuruf = idHuruf;
    }

    public String getNamaHuruf() {
        return namaHuruf;
    }

    public void setNamaHuruf(String namaHuruf) {
        this.namaHuruf = namaHuruf;
    }

    public String getDescHuruf() {
        return descHuruf;
    }

    public void setDescHuruf(String descHuruf) {
        this.descHuruf = descHuruf;
    }

    public String getGambarPath() {
        return gambarPath;
    }

    public void setGambarPath(String gambarPath) {
        this.gambarPath = gambarPath;
    }

    public String getGambarBlob() {
        return gambarBlob;
    }

    public void setGambarBlob(String gambarBlob) {
        this.gambarBlob = gambarBlob;
    }

    public int getIdMakhroj() {
        return idMakhroj;
    }

    public void setIdMakhroj(int idMakhroj) {
        this.idMakhroj = idMakhroj;
    }

    public String getIconHuruf() {
        return iconHuruf;
    }

    public void setIconHuruf(String iconHuruf) {
        this.iconHuruf = iconHuruf;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(idHuruf);
        parcel.writeString(namaHuruf);
        parcel.writeString(descHuruf);
        parcel.writeString(gambarPath);
        parcel.writeString(gambarBlob);
        parcel.writeString(iconHuruf);
        parcel.writeInt(idMakhroj);
    }
}
