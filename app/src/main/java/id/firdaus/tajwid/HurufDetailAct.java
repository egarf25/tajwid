package id.firdaus.tajwid;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import id.firdaus.tajwid.entities.Huruf;

public class HurufDetailAct extends AppCompatActivity {

    ImageView ic_back;
    private Intent intent;
    private Huruf huruf;
    private TextView text_title;
    private TextView text_desc;
    private ImageView header_makhraj;
    private String jenis_gambar;
    private TextView text_hasil_uji;
    private long startTime = 0;
    private long endTime = 0;
    private long startMemory = 0;
    private long endMemory = 0;
    private TextView text_size_hasil;
    private TextView text_cpu_hasil;
    private TextView text_memory_hasil;
    private TextView text_time_hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huruf_detail);
        startTime = System.currentTimeMillis();
        startMemory = getUsedMemorySize();

        text_title = findViewById(R.id.text_title);
        text_desc = findViewById(R.id.text_desc);
        header_makhraj = findViewById(R.id.header_makhraj);
        text_hasil_uji = findViewById(R.id.text_hasil_uji);

        text_size_hasil = findViewById(R.id.text_size_hasil);
        text_cpu_hasil = findViewById(R.id.text_cpu_hasil);
        text_memory_hasil = findViewById(R.id.text_memory_hasil);
        text_time_hasil = findViewById(R.id.text_time_hasil);

        intent = getIntent();
        huruf = intent.getParcelableExtra("huruf");
        jenis_gambar = intent.getStringExtra("jenis_gambar");

        if (jenis_gambar.equals("path")){
            text_hasil_uji.setText("Hasil Uji Path");
        } else if (jenis_gambar.equals("blob")){
            text_hasil_uji.setText("Hasil Uji Blob");
        }

        if (huruf == null){
            startActivity(new Intent(this, HomeAct.class));
        }

        setTitle(huruf.getNamaHuruf());
        text_title.setText(huruf.getNamaHuruf());
        text_desc.setText(huruf.getDescHuruf());

        ic_back = findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        try {
            new HurufGet(huruf.getIdHuruf(), jenis_gambar).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class HurufGet extends AsyncTask<Void, Integer, String> {
        private int id;
        private String jenis_gambar;

        public HurufGet(int id, String jenis_gambar) {
            this.id = id;
            this.jenis_gambar = jenis_gambar;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String json_String = "";
            URL url = null;
            HttpURLConnection urlConnection = null;
            InputStream in = null;
            try {
                String uri = getString(R.string.host)+"huruf_get.php?id="+this.id+"&gambar="+this.jenis_gambar;
                url = new URL(uri);
                urlConnection = (HttpURLConnection) url.openConnection();
//                in = new BufferedInputStream(urlConnection.getInputStream());
                Log.d(HurufDetailAct.class.getSimpleName(), uri);

                BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line;
                while ((line = rd.readLine()) != null) {
                    json_String += line;
                    Log.d(HurufDetailAct.class.getSimpleName(), json_String);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null){
                    urlConnection.disconnect();
                }
            }
            return json_String;
        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);

            if (this.jenis_gambar.equals("path")){
                String urlHuruf = getString(R.string.host) +"uploads/" + s;
                Log.d(HurufDetailAct.class.getSimpleName(), "urlHuruf:" + urlHuruf);

                Picasso.get().load(urlHuruf)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                //endTime = System.currentTimeMillis();
                                //endMemory = getUsedMemorySize();
                                header_makhraj.setImageBitmap(bitmap);
                                endTime = System.currentTimeMillis();
                                endMemory = getUsedMemorySize();

                                text_time_hasil.setText("Duration:" + (endTime - startTime) + " milisecond");
                                text_memory_hasil.setText("Memory:" + (endMemory - startMemory)/8 + " byte");
                                //text_size_hasil.setText("Size:" + getSize(s) + " byte");
                                text_cpu_hasil.setVisibility(View.GONE);

                                saveLogFile("path", s);
                                Log.d(HurufDetailAct.class.getSimpleName(), "onBitmapLoaded:" + bitmap.getByteCount());
                            }

                            @Override
                            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                                Log.d(HurufDetailAct.class.getSimpleName(), "onBitmapFailed:" + e.getMessage());

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                Log.d(HurufDetailAct.class.getSimpleName(), "onPrepareLoad:");
                            }
                        });
            } else if (this.jenis_gambar.equals("blob")){
                byte[] decodedString = Base64.decode(s, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                header_makhraj.setImageBitmap(decodedByte);
                endTime = System.currentTimeMillis();
                endMemory = getUsedMemorySize();

                text_time_hasil.setText("Duration:" + (endTime - startTime) + " milisecond");
                text_memory_hasil.setText("Memory:" + (endMemory - startMemory)/8 + " byte");
                //text_size_hasil.setText("Size:" + getSize(s) + " byte");
                text_cpu_hasil.setVisibility(View.GONE);

                saveLogFile("blob", s);
            }


        }
    }

    public static long getUsedMemorySize() {
        long freeSize = 0L;
        long totalSize = 0L;
        long usedSize = -1L;
        long usedMemInMB = -1L;
        try {
            Runtime info = Runtime.getRuntime();
            freeSize = info.freeMemory();
            totalSize = info.totalMemory();
            Log.d(HurufDetailAct.class.getSimpleName(), "totalSize:"+totalSize);
            usedSize = totalSize - freeSize;

            usedMemInMB=(info.totalMemory() - info.freeMemory()) / 1048576L;
            final long maxHeapSizeInMB=info.maxMemory() / 1048576L;
            final long availHeapSizeInMB = maxHeapSizeInMB - usedMemInMB;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return usedSize;
    }

    public int getSize(String source){
        byte[] byteArray = new byte[0];
        try {
            byteArray = source.getBytes("UTF-16BE");
            return byteArray.length;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void saveLogFile(String jenisGambar, String s) {
        String log = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date resultDate = new Date(System.currentTimeMillis());
        log += sdf.format(resultDate);
        log += ":";
        if (huruf != null && huruf.getNamaHuruf() != null){
            log += huruf.getNamaHuruf();
            log += ";";
        }
        log += "duration:"+(endTime - startTime)+" milisecond;";
        log += "memory:"+(endMemory - startMemory)+" byte;";
        log += "size:"+(getSize(s))+" byte;\n";
        save(jenisGambar, log);
    }

    public void save(String gambar, String text) {
        FileOutputStream fos = null;

        File dir = new File(Environment.getExternalStorageDirectory(
                ).getAbsolutePath()+"/"+getString(R.string.app_name));
        File outputFile = new File(dir, gambar+"_log.txt");
        try {
            if (!dir.exists()){
                dir.mkdirs();
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        try {
            fos = new FileOutputStream(outputFile, true);
            fos.write(text.getBytes());
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void checkApplicationPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (! ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 102);
            }
        }
    }
}
